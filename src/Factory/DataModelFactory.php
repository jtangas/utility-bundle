<?php
/**
 * Created by PhpStorm.
 * User: jtangas
 * Date: 12/20/17
 * Time: 11:03 AM
 */

namespace Jtangas\UtilityBundle\Factory;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

class DataModelFactory
{
    protected $entityManager;

    protected $formatters = [];

    protected $i = 0;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function get($model, $unique = false)
    {
        if (!class_exists($model)) {
            throw new \Exception('Requestd model not found for formatting: ' . $model);
        }

        if (!$unique && isset($this->formatters[$model])) {
            return $this->formatters[$model];
        }

        $formatter = new $model($this, $this->entityManager);

        if (!$unique) {
            $this->formatters[$model] = $formatter;
        }

        return $formatter;
    }

    public function format($entities, $model = null, $format = null, $options = null)
    {
        if (is_null($entities)) {
            return null;
        }

        if (!is_object($entities) && !is_array($entities)) {
            return $entities;
        }

        if (!$entities) {
            return [];
        }

        $unwrap = false;
        if (!is_array($entities) && (!$entities instanceof \ArrayAccess)) {
            $unwrap = true;
            $entities = [$entities];
        }

        $ret = [];
        foreach ($entities as $entity) {
            try {
                $useModel = $model ?: $entity->getFormatterModel();
                $formatter = $this->get($useModel, false);
                $ret[] = $formatter->format($entity, $format, $options);
            } catch (EntityNotFoundException $e) {
                $ret[] = "Entity has been deleted";
            }
        }

        return $unwrap ? $ret[0] : $ret;
    }
}