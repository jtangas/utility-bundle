<?php
/**
 * Created by PhpStorm.
 * User: jtangas
 * Date: 12/20/17
 * Time: 10:40 AM
 */

namespace Jtangas\UtilityBundle;


use Exception;
use Symfony\Component\HttpFoundation\Request;

class Utility
{
    const CHARS_DISTINCT = 'lowercase-distinct';
    const CHARS_LOWER = 'lowercase-alpha';
    const CHARS_ALPHA = 'upper-lower-alpha';
    const CHARS_ALPHANUM = 'upwer-lower-digits';
    const CHARS_DIGITS = 'digits';
    const CHARSETS = [
        self::CHARS_DISTINCT    => 'abcdefghijklmnopqrstuvwxyz',
        self::CHARS_LOWER       => 'abcdefghijklmnopqrstuvwxyz',
        self::CHARS_ALPHA       => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
        self::CHARS_ALPHANUM    => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
        self::CHARS_DIGITS      => '0123456789'
    ];

    static function getClientIp(Request $request = null)
    {
        $ip = '';
        if ( !is_null($request)) {
            $ip = $request->getClientIp();
        }
        if (empty($ip) && array_key_exists('HTTP_X_FORWARDED_FOR',
                $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])
        ) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        if (empty($ip) && array_key_exists('REMOTE_ADDR', $_SERVER) && !empty($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        if (empty($ip) && array_key_exists('DEFAULT_REMOTE_ADDR',
                $_SERVER) && !empty($_SERVER['DEFAULT_REMOTE_ADDR'])
        ) {
            $ip = $_SERVER['DEFAULT_REMOTE_ADDR'];
        }
        if (empty($ip) && array_key_exists('HTTP_CLIENT_IP', $_SERVER) && !empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        if (empty($ip) && array_key_exists('REAL_IP', $_SERVER) && !empty($_SERVER['REAL_IP'])) {
            $ip = $_SERVER["REAL_IP"];
        }
        if (empty($ip) && !is_null($request)) {
            $h = $request->headers;
            if ($h->has('X-Forwarded-For')) {
                $ip = $h->get('X-Forwarded-For', null, true);
            }
        }

        return empty($ip) ? false : trim(current(explode(',', $ip)));
    }

    public static function randString($length = 8, $charSet = self::CHARS_DISTINCT)
    {
        $source = self::CHARSETS[$charSet];
        $ret = '';
        $maxIdx = strlen($source) - 1;
        for ($i = 0; $i < $length; $i++) {
            $useIdx = random_int(0, $maxIdx);
            $ret .= $source[$useIdx];
        }

        return $ret;
    }

    public static function strposa($haystack, $needles = [], $offset = 0)
    {
        $chr = [];
        foreach ($needles as $needle) {
            $res = strpos($haystack, $needle, $offset);
            if ($res !== false) {
                $chr[$needle] = $res;
            }
        }

        if (empty($chr)) {
            return false;
        }

        return min($chr);
    }

    static function strDump()
    {
        $args = func_get_args();
        ob_start();
        call_user_func_array('var_dump', $args);
        $dump = ob_get_contents();
        ob_end_clean();

        return $dump;
    }

    static function errDump()
    {
        $args = func_get_args();
        $dump = call_user_func_array(['self', 'strDump'], $args);
        error_log($dump);
    }

    static function exceptionTraceDump(Exception $exception)
    {
        $cleanTrace = array_map(
            function ($traceItem) {
                return join(' : ',
                    array_filter(
                        array_map(function ($field) use ($traceItem) {
                            return Utility::get($traceItem[$field], null);
                        }, ['file', 'line', 'class', 'function'])
                    )
                );
            },
            $exception->getTrace()
        );

        self::errDump(
            get_class($exception),
            $exception->getMessage(),
            $cleanTrace
        );
    }

    static function get(&$ref, $default = null)
    {
        return isset($ref) ? $ref : $default;
    }

    static function words2lowCamel($words)
    {
        return lcfirst(self::words2upCamel($words));
    }

    static function words2upCamel($words)
    {
        return implode('', array_map('ucfirst', $words));
    }

    static function words2lowSnake($words)
    {
        return strtolower(implode('_', $words));
    }

    static function words2upSnake($words)
    {
        return strtoupper(implode('_', $words));
    }

    static function camel2words($string)
    {
        return preg_split('/(?<=[a-z])(?=[A-Z])/x', $string);
    }

    static function slug2upCamel($string)
    {
        return self::words2upCamel(explode('-', $string));
    }

    static function snake2upCamel($string)
    {
        return self::words2upCamel(explode('_', $string));
    }

    static function snake2lowCamel($string)
    {
        return lcfirst(self::words2lowCamel(explode('_', $string)));
    }

    static function camel2lowSnake($string)
    {
        return self::words2lowSnake(self::camel2words($string));
    }

    static function camel2upSnake($string)
    {
        return self::words2upSnake(self::camel2words($string));
    }

    static function snake2Words($string)
    {
        $string = str_replace('_', ' ', $string);

        return trim($string);
    }
}