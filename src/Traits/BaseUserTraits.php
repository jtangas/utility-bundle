<?php
/**
 * Created by PhpStorm.
 * User: jtangas
 * Date: 12/20/17
 * Time: 12:41 PM
 */

namespace Jtangas\UtilityBundle\Traits;


use Doctrine\ORM\Mapping as ORM;
use Jtangas\UtilityBundle\DataModel\UserResponse;

trait BaseUserTraits
{
    /**
     * @var string
     * @ORM\Column(name="time_zone", type="string", length=100, nullable=false, options={"default":"America/Los_Angeles"})
     */
    protected $timeZone = 'America/Los_Angeles';

    /**
     * @var string
     * @ORM\Column(name="requested_email", type="text", nullable=true)
     */
    protected $requestedEmail;

    /**
     * @var string
     * @ORM\Column(name="reset_token", type="text", nullable=true)
     */
    protected $resetToken;

    /**
     * @var \DateTime
     * @ORM\Column(name="reset_token_expiration", type="datetime", nullable=true)
     */
    protected $resetTokenExpiration;

    function getFormatterModel()
    {
        return UserResponse::class;
    }

    public function getTimeZone()
    {
        return $this->timeZone;
    }

    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;
    }

    public function getRequestedEmail()
    {
        return $this->requestedEmail;
    }

    public function setRequestedEmail($requestedEmail)
    {
        $this->requestedEmail = $requestedEmail;
    }

    public function getResetToken()
    {
        return $this->resetToken;
    }

    public function setResetToken($resetToken)
    {
        $this->resetToken = $resetToken;
    }

    public function getResetTokenExpiration()
    {
        return $this->resetTokenExpiration;
    }

    public function setResetTokenExpiration($resetTokenExpiration)
    {
        $this->resetTokenExpiration = $resetTokenExpiration;
    }
}