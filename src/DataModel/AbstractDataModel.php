<?php
/**
 * Created by PhpStorm.
 * User: jtangas
 * Date: 12/20/17
 * Time: 11:11 AM
 */

namespace Jtangas\UtilityBundle\DataModel;


use Doctrine\ORM\EntityManagerInterface;
use Jtangas\UtilityBundle\Factory\DataModelFactory;
use Jtangas\UtilityBundle\Interfaces\ModelableInterface;

abstract class AbstractDataModel implements DataModelInterface
{
    const FORMAT_ENTRY = 'formatEntry';
    const FORMAT_LIST = 'formatList';
    const FORMAT_EXTRA = 'formatExtra';
    const FORMAT_SHORT = 'formatShort';

    protected $factory;

    protected $em;

    public $currentFormat;

    protected static $formatMappings = [
        self::FORMAT_ENTRY,
        self::FORMAT_LIST,
        self::FORMAT_EXTRA,
        self::FORMAT_SHORT,
    ];

    public function __construct(DataModelFactory $factory, EntityManagerInterface $em)
    {
        $this->factory  = $factory;
        $this->em       = $em;
    }

    public function format(ModelableInterface $data = null, $format = null, $options = [])
    {
        if (is_null($data) || !($data instanceof ModelableInterface)) {
            return null;
        }

        $func = (
            $format
            && in_array($format, self::$formatMappings)
            && method_exists($this, $format)
        )
            ? $format
            : 'formatDefault';

        return $this->{$func}($data, $options);
    }
}