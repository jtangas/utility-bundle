<?php
/**
 * Created by PhpStorm.
 * User: jtangas
 * Date: 12/20/17
 * Time: 11:21 AM
 */

namespace Jtangas\UtilityBundle\DataModel;


use FOS\UserBundle\Model\UserInterface;

class UserResponse extends AbstractDataModel
{
    const FORMAT_OPTIONS = 'user-format-options';

    const FORMAT_SUMMARY = 'format-summary';
    const FORMAT_SHORT   = 'format-short';

    protected function formatDefault(UserInterface $user, $options = [])
    {
        if (isset($options[self::FORMAT_OPTIONS])) {
            if (in_array(self::FORMAT_SUMMARY, $options[self::FORMAT_OPTIONS])) {
                return $this->formatSummary($user, $options);
            }
            if (in_array(self::FORMAT_SHORT, $options[self::FORMAT_OPTIONS])) {
                return $this->formatShort($user, $options);
            }
        }

        return $this->formatJson($user, $options);
    }

    protected function formatJson(UserInterface $user, $options = [])
    {
        $ret = [
            'response_format'   => __FUNCTION__,
            'user_id'           => $user->getId(),
            'username'          => $user->getUsername(),
            'email'             => $user->getEmail(),
            'time_zone'         => $user->getTimeZone(),
            'created_at'        => $user->getCreatedAt(),
            'modified_at'       => $user->getModifiedAt(),
        ];

        return $ret;
    }

    protected function formatShort(UserInterface $user, $options = [])
    {
        return $user->getUsername();
    }

    protected function formatSummary(UserInterface $user, $options = [])
    {
        $ret = [
            'response_format'   => __FUNCTION__,
            'user_id'           => $user->getId(),
            'username'          => $user->getUsername(),
            'email'             => $user->getEmail(),
            'time_zone'         => $user->getTimeZone(),
            'created_at'        => $user->getCreatedAt(),
            'modified_at'       => $user->getModifiedAt(),
        ];

        return $ret;
    }
}